package com.auth.shiro.config;

import com.auth.shiro.entity.Permission;
import com.auth.shiro.entity.Role;
import com.auth.shiro.entity.User;
import com.auth.shiro.service.UserService;
import com.auth.shiro.util.JWTUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Objects;

/**
 * 自定义 Realm
 *
 * @author: mhSui 2020/07/02
 */
@Slf4j
public class MyRealm extends AuthorizingRealm {

    @Autowired
    private UserService userService;

    /**
     * 必须重写此方法，不然Shiro会报错
     */
    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof JWTToken;
    }

    /**
     * 当前登录成功的用户授予权限和分配角色
     * 此方法调用hasRole,hasPermission的时候才会进行回调
     * 权限信息.(授权):
     * @param
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        //获取登录用户名
        String name = (String) principalCollection.getPrimaryPrincipal();
        //根据用户名去数据库查询用户信息
        User user = userService.findByUserName(name);
        //添加角色和权限
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        for (Role role : user.getRoleList()) {
            //添加角色
            simpleAuthorizationInfo.addRole(role.getRoleName());
            //添加权限
            for (Permission permission : role.getPermissions()) {
                simpleAuthorizationInfo.addStringPermission(permission.getPermission());
            }
        }
        return simpleAuthorizationInfo;
    }


    /**
     * 验证当前登录的用户，获取认证信息
     *
     * @param
     * @return
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        log.info("开始验证登陆用户");
        String token = (String) authenticationToken.getCredentials();
        //解密获得username,用于数据库进行对比
        String username = JWTUtil.getUsername(token);
        if (username == null){
            throw new AuthenticationException("token invalid");
        }
        //通过username从数据库中查找 BaseAccount
        //实际项目中，这里可以根据实际情况做缓存，如果不做，Shiro自己也是有时间间隔机制，2分钟内不会重复执行该方法
        User user = this.userService.findByUserName(username);
        if (Objects.isNull(user)){
            throw new AuthenticationException("User didn't existed!");
        }
        if (!JWTUtil.verify(token,username,user.getPassword())){
            throw new AuthenticationException("Username or Password error");
        }
        return new SimpleAuthenticationInfo(user, token, user.getUsername());
    }
}
