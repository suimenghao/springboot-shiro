package com.auth.shiro.mapper;

import com.auth.shiro.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author: mhSui 2020/07/03
 */
@Mapper
public interface UserMapper {

    User findByUserName(@Param("username") String username);

}
