package com.auth.shiro.controller;

import com.auth.shiro.config.JWTToken;
import com.auth.shiro.entity.User;
import com.auth.shiro.service.UserService;
import com.auth.shiro.util.EncoderUtil;
import com.auth.shiro.util.JWTUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author: mhSui 2020/07/03
 */
@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 测试登陆
     *
     * @param user
     * @return
     */
    @PostMapping("/login")
    public String login(@RequestBody User user) {
        String encodedPassword = EncoderUtil.sha256(user.getPassword());
        String token = JWTUtil.sign(user.getUsername(), encodedPassword);
        SecurityUtils.getSubject().login(new JWTToken(token));
        User user1 = (User) SecurityUtils.getSubject().getPrincipal();
        log.info(user1.getUsername());
        return token;
    }

    /**
     * 测试登陆拦截
     *
     * @param username
     * @return
     */
    @GetMapping("/getUserInfo")
    public User gerUserInfo(@RequestParam("username") String username) {
        return userService.findByUserName(username);
    }
}
