package com.auth.shiro.service.impl;

import com.auth.shiro.entity.User;
import com.auth.shiro.mapper.UserMapper;
import com.auth.shiro.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: mhSui 2020/07/03
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;


    @Override
    public User findByUserName(String username) {
        try {
            return this.userMapper.findByUserName(username);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
