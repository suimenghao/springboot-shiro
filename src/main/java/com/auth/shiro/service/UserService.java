package com.auth.shiro.service;

import com.auth.shiro.entity.User;

/**
 * @author: mhSui 2020/07/03
 */
public interface UserService {

    User findByUserName(String username);
}
