package com.auth.shiro.entity;

import lombok.Data;

import java.util.List;

/**
 * @author: mhSui 2020/07/03
 */
@Data
public class User {

    private Integer id;

    private String username;

    private String password;

    private Integer roleId;

    private List<Role> roleList;
}
