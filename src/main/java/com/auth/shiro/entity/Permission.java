package com.auth.shiro.entity;

import lombok.Data;

/**
 * 权限
 *
 * @author suimenghao 2020/08/17
 */
@Data
public class Permission {

    private Long id;

    private String permission;
}
