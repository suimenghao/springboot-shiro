package com.auth.shiro.entity;

import lombok.Data;

import java.util.List;

/**
 * 权限
 *
 * @author suimenghao 2020/08/17
 */
@Data
public class Role {

    private Integer id;

    private String roleName;

    private String permission;

    private List<Permission> permissions;
}
